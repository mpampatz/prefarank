
# Εδώ ορίζεται μια δομή τύπου database για την αποθήκευση των accounts
# και των διαφόρων χαρακτηριστικών τους. Οι μέθοδοι είναι εξειδικευμενες
# να διαβάζουν τα δεδομένα των παρτίδων όπως αυτά καταχωρούνται στο 
# αρχείο epilogi.php.

class DictOfAccounts(dict):
    def __init__(self):
        self.IDdict = {}
        self.size = 0

    def insertAccount (self, name : str):
        if name not in self.keys():
            ID = self.size
            self[name] = \
                    { 'ID' : ID \
                    , 'dianomes' : 0 \
                    , 'partides' : 0 \
                    , 'kapikia' : 0 \
                    , 'vpgmres' : 0 \
                    , 'vngmres' : 0 \
                    , 'moyenne' : 0 \
                    }
            self.IDdict[ID] = name
            self.size += 1

    def getAttributeById(self, ID, attr):
        if ID in range(self.size):
            name = self.IDdict[ID]
            return self[name][attr]

    def insertListOfAttributes(self, inlist, attr):
        if len(inlist) == self.size:
            for ID in range(self.size):
                name = self.IDdict[ID]
                self[name][attr] = inlist[ID]

    def getListOfAttributes(self,  attr):
        outlist = []
        for ID in range(self.size):
            name = self.IDdict[ID]
            outlist.append(self[name][attr])
        return outlist

    def _prosmetrisiAccountPartidas(self, pektes, dianomes):
        for pektis in pektes:
            self.insertAccount(pektis)

    def _prosmetrisiPartidas(self, pektes, dianomes):
        for pektis in pektes:
            self[pektis]['partides'] += 1

    def _prosmetrisiDianomonPartidas(self, pektes, dianomes):
        for pektis in pektes:
            self[pektis]['dianomes'] += len(dianomes)

    def _prosmetrisiKerdonPartidas(self, pektes, dianomes):
        for dianomi in dianomes:
            for (pektis, kapikia) in zip(pektes, kerdiDianomis(dianomi)):
                self[pektis]['kapikia'] += kapikia

    def _ipologismosMoyenne(self):
        for item in self.values():
            item['moyenne'] = item['kapikia'] / item['dianomes']

    def preProc(self, rawPartides):
        for partida in rawPartides:
            if  partida['d'] == []:
                continue
            pektes = [ partida['p1'], partida['p2'], partida['p3'] ]
            dianomes = partida['d']

            self._prosmetrisiAccountPartidas(pektes, dianomes)
            self._prosmetrisiPartidas(pektes, dianomes)
            self._prosmetrisiDianomonPartidas(pektes, dianomes)
            self._prosmetrisiKerdonPartidas(pektes, dianomes)

        self._ipologismosMoyenne()

    def _sortNamesBy(self, skey : str, srev=True):
        return sorted(self.items(), key=lambda x: x[1][skey], reverse=srev)

    def printAccount(self, key):
        print( "%-20s : %5i %4i %5.0f %7.2f %7.2f %7.0f" % \
                ( key \
                , self[key]['dianomes'] \
                , self[key]['partides'] \
                , self[key]['kapikia'] \
                , self[key]['moyenne'] \
                , self[key]['vpgmres'] \
                , self[key]['vngmres'] \
                ) \
                )

    def printSortedBy(self, skey : str, srev=True, dfilt=int(1000)):
        for (key,attrs) in self._sortNamesBy(skey, srev):
            if attrs['dianomes'] > dfilt:
                self.printAccount(key)






def kerdiDianomis(dianomi):
        dKases = [ int(dianomi[k]) for k in [ 'k1', 'k2', 'k3' ] ]
        dPikia = [ int(dianomi[m]) for m in [ 'm1', 'm2', 'm3' ] ]
        dKasaMean = sum(dKases)/3
        return [ dKases[i] + dPikia[i] - dKasaMean for i in range(3) ]

