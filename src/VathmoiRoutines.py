# Εδώ ορίζονται ο παλιος και ο νέος τρόπος υπολογισμού της βαθμολογίας.
# Και στις δύο μεθόδους πρέπει να λυθεί ένα γραμμικό σύστημα.
# Σε κάθε περίπτωση κατασκευάζεται το μητρώο και το δεξί μέρος και στη
# συνέχεια το γραμμικό σύστημα λύνεται με κλίση της gmres για sparce
# μητρώα.

from scipy.sparse import dok_matrix
from scipy.sparse.csgraph import connected_components
from scipy.sparse.csgraph import laplacian
from scipy.sparse.linalg import gmres
from numpy import ndarray
from numpy import asarray

def ipologismosAdjacencyMatrix(rawPartides, DoA):
    size = DoA.size
    Amat = dok_matrix((size, size), dtype=int)
    for partida in rawPartides:
        if  partida['d'] == []:
            continue
        pektes = [ partida['p1'], partida['p2'], partida['p3'] ]
        dianomesCount = len(partida['d'])
        for pektisI in pektes:
            for pektisJ in pektes:
                i = DoA[pektisI]['ID']
                j = DoA[pektisJ]['ID']
                Amat[i,j] += dianomesCount
    return Amat



def vathmosPalioGMRES(rawPartides, DoA):
    size = DoA.size
    AdjacencyMatrix = ipologismosAdjacencyMatrix(rawPartides, DoA)
    # το αφήνω να χρησιμοποιήσει όποια δομή sparse θέλει...
    DegreeMatrix = laplacian(AdjacencyMatrix) + AdjacencyMatrix
    # Ο παλιός τρόπος λύνει το σύστημα με μητρώο
    #       2*Degree - Adjacency
    # το οποίο δεν είναι ανώμαλο.
    A = DegreeMatrix.multiply(2) - AdjacencyMatrix
    # ...αλλά πριν καλέσω τη gmres το αλλάζω το μητρώο σε μια δομή
    # που νομίζω ότι είναι η πιο αποδοτική.
    A = A.tocsr()
    rhs = DegreeMatrix.dot(DoA.getListOfAttributes('moyenne'))
    return gmres(A, rhs)

def vathmosNeoGMRES(rawPartides, DoA):
    # The problem has 2 free parameters for us to set arbitrarily.
    # Choose...
    spread = 100
    # and..
    center = 1700
    # to get ratings similar to ELO.


    #construct global matrix and rhs
    size = DoA.size
    AdjacencyMatrix = ipologismosAdjacencyMatrix(rawPartides, DoA)

    # Η νέα μέθοδος λύνει το ίδιο σύστημα με την παλια αλλά με μητρώο
    #       Degree - Adjacency = Laplacian
    # Και στις δύο μεθόδους το δεξί μέρος είναι το ίδιο
    # rhs_i = 3*kapikia_i = Degree_ii*moyenne_i
    LaplacianMatrix = laplacian(AdjacencyMatrix)
    rhs = asarray([ spread*3*kapikia for kapikia in DoA.getListOfAttributes('kapikia') ])

    #Substitute with "conditions".
    #First find all connected components and null eigenvectorors.
    conCompCount, conCompLabel = connected_components(AdjacencyMatrix)
    eigenvectors = ndarray((conCompCount, size))
    for ieig in range(conCompCount):
        eigenvectors[ieig] = [ 1 if label == ieig else 0 for label in conCompLabel ]

    #Now overwrite one equation for every connected component.
    LaplacianMatrix = LaplacianMatrix.todok()
    for ieig in range(conCompCount):
        ipos = list(conCompLabel).index(ieig)
        icou = list(conCompLabel).count(ieig)
        for jpos in range(size):
            LaplacianMatrix[ipos,jpos] = eigenvectors[ieig][jpos]
        rhs[ipos] = center*icou

    #Ready, steady..
    LaplacianMatrix = LaplacianMatrix.tocsr()
    #...go!
    return gmres(LaplacianMatrix, rhs)

