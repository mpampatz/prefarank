import sys
from DictOfAccounts import *
from VathmoiRoutines import *

accounts = DictOfAccounts()

rawdata = []

# Κατεβάζω το αρχείο δεδομένων πετώντας την αχρηστη πληροφορία με την παρακάτω εντολή:
#   curl "www.prefadoros.gr/arxio/epilogi.php?limit=11000&skip=1000" | sed 's/,"e":\[[^]]*\]//g' > epilogi.cut4
# Το αρχείο αυτό μπαίνει όρισμα την κλήση του main.py. π.χ.:
#   python -i main.py epilogi.cut4


infilename = sys.argv[1]
print('Loading file ', infilename, ' ...')
infile = open(infilename,'r')
null = ''
rawdata = eval(infile.read())
infile.close()

print('Preprocessing...')
accounts.preProc(rawdata)

print('Palio sistima, methodos GMRES...')
vathmos, info = vathmosPalioGMRES(rawdata, accounts)
if info == 0:
    accounts.insertListOfAttributes(vathmos, 'vpgmres')
else:
    print( 'vathmosPalioGMRES: NO Convergence' )

print('Neo sistima, methodos GMRES...')
vathmos, info = vathmosNeoGMRES(rawdata, accounts)
if info == 0:
    accounts.insertListOfAttributes(vathmos, 'vngmres')
else:
    print( 'vathmosNeoGMRES: NO Convergence' )

accounts.printSortedBy('vngmres', dfilt=0)



